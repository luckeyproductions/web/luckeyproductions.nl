<!DOCTYPE html>
<html style="background-color: black;">
<head>
    <meta charset="utf-8">
    <title>LucKey Radio</title>
    <link rel="stylesheet" href="https://luckeyproductions.nl/stylesheet.css">
</head>
<body style="width: 100vw; height: 100vh; margin: 0;">
    <audio autoplay onended="window.location.reload(true)"><source src="<?php
    $songs = array();

    foreach(scandir(getcwd()) as $k => $file)
        if (strpos($file, "-"))
            $songs[count($songs)] = $file;

    if (count($songs))
        echo $songs[rand() % count($songs)];
?>"></audio>
</body>
</html>
