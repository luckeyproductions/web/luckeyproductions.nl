<!DOCTYPE html>
<?php
    $play = array_key_exists("play", $_GET);
?>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width">

    <title>Random sound</title>

    <link rel="stylesheet" href="https://luckeyproductions.nl/stylesheet.css">
  </head>

  <body onload="play()">
  </body>
<script>
let AudioContext = window.AudioContext || window.webkitAudioContext;
let audioCtx = new AudioContext();
var mainGain;
// Stereo
let channels = 2;

function play() {
<?
    if ($play)
        echo "window.location.replace('play.php');";
?>
    document.addEventListener(visibilityChange, handleVisibilityChange, false);

    var carrier, merger, modulator, lastModulator;

	window.AudioContext = window.AudioContext || window.webkitAudioContext;
    merger = audioCtx.createChannelMerger(2);
    window.mainGain = audioCtx.createGain ? audioCtx.createGain() : audioCtx.createGainNode();

    var baseFreq = Math.pow(Math.random() * 9000, 0.2) * 23 + 5;
    var mods = Math.floor(Math.sqrt(Math.random() * 420)) + 1;
    var baseTypeInt = Math.floor(Math.random() * 2);
    switch (baseTypeInt) {
    case 0: default: baseType = "sine"; break;
    case 1: baseType = "triangle"; break;
    }

    for (var g = 0; g < channels; ++g)
    {
	    var gainNode = audioCtx.createGain ? audioCtx.createGain() : audioCtx.createGainNode();
        gainNode.gain.value = Math.max(0.1, 1.0 - 0.42 * (Math.sqrt(baseFreq) / 10));

        for (var m = 0; m < mods - (g * Math.floor(Math.random() * mods * 0.5)); ++m)
        {
           var type;
            switch (Math.floor(Math.random() * 5)) {
            case 0: default: type = "sine"; break;
            case 1: type = "square"; break;
            case 2: type = "sawtooth"; break;
            case 3: type = "triangle"; break;
            }

            modulator = new Modulator(type, Math.pow(Math.random() * mods + 0.1, 0.125 + Math.random() * (mods - m) % 3),  Math.random() * baseFreq * 0.666 + 23);

            if (m == 0)
            {
                carrier = new Carrier(baseType, baseFreq * (1.0 + 0.01 * Math.random() * g));
                modulator.gain.connect(carrier.osc.frequency);
                gainNode.gain.value *= 1 - (baseType != "sine") * 0.5;
            }
            else if (m % 3 != 0 && (Math.random() > 0.666))
            {
                modulator.osc.frequency.value = Math.random() * modulator.osc.frequency.value + Math.sqrt(modulator.osc.frequency.value);
                modulator.gain.gain.value = Math.random() * Math.sqrt(lastModulator.osc.frequency.value * 42) + 0.1;
                modulator.gain.connect(lastModulator.gain.gain);
            }
            else
            {
                modulator.gain.gain.value = Math.random() * Math.pow(Math.min(235.0, lastModulator.gain.gain.value), Math.min(5, Math.pow(lastModulator.osc.frequency.value, 1 / 6))) + 0.1;
                modulator.gain.connect(lastModulator.osc.frequency);
            }

            lastModulator = modulator;
        }

        carrier.osc.connect(gainNode);
        gainNode.connect(merger, 0, g);
    }

    merger.connect(mainGain);
    mainGain.connect(audioCtx.destination);
}

// A modulator has an oscillator and a gain
function Modulator (type, freq, gain) {
  this.osc = audioCtx.createOscillator();
  this.gain = audioCtx.createGain();
  this.osc.type = type;
  this.osc.frequency.value = freq;
  this.gain.gain.value = gain;
  this.osc.connect(this.gain);
  this.osc.start(0);
}
Modulator.prototype.toString = function () {
    return "freq="+this.osc.frequency.value.toFixed(1)+
          " amp="+this.gain.gain.value.toFixed(0);
}

function Carrier (type, freq) {
  this.osc = audioCtx.createOscillator();
  this.gain = audioCtx.createGain();
  this.osc.type = type;
  this.osc.frequency.value = freq;
  this.osc.connect(this.gain);
  this.osc.start(0);
}
Carrier.prototype.toString = function () {
    return "freq="+this.osc.frequency.value.toFixed(1)+
          " amp="+this.gain.gain.value.toFixed(1);
}

var hidden, visibilityChange;
if (typeof document.hidden !== "undefined") { // Opera 12.10 and Firefox 18 and later support
  hidden = "hidden";
  visibilityChange = "visibilitychange";
} else if (typeof document.msHidden !== "undefined") {
  hidden = "msHidden";
  visibilityChange = "msvisibilitychange";
} else if (typeof document.webkitHidden !== "undefined") {
  hidden = "webkitHidden";
  visibilityChange = "webkitvisibilitychange";
}

function handleVisibilityChange() {
  if (document[hidden]) {
    mainGain.gain.value = 0;
  } else {
    mainGain.gain.value = 1;
  }
}
  </script>
</html>
