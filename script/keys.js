document.addEventListener('keypress', handleKeypress);


function handleKeypress(e)
{
  switch (e.keyCode)
  {
  case 70: toggleFullscreen();
      break;
  default: return;
  }
}

function launchFullscreen(element)
{
    if (element.requestFullScreen)
        element.requestFullScreen();
    else if (element.mozRequestFullScreen)
        element.mozRequestFullScreen();
    else if (element.webkitRequestFullScreen)
        element.webkitRequestFullScreen();
}

function cancelFullscreen()
{
    if (document.cancelFullScreen) { document.cancelFullScreen(); }
    else if (document.mozCancelFullScreen) { document.mozCancelFullScreen(); }
    else if (document.webkitCancelFullScreen) { document.webkitCancelFullScreen(); }
}

function isFullscreen()
{
    return window.fullScreen ||
           document.fullscreenEnabled    ||
           document.mozFullscreenEnabled ||
           document.webkitFullscreenEnabled ?
           true : false;
}

function toggleFullscreen()
{
    if (isFullscreen())
        cancelFullscreen();
    else
        launchFullscreen(document.documentElement);
}
