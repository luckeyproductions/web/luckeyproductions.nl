<!DOCTYPE html>
<html style="background-color: black;">
<head>
    <meta charset="utf-8">
    <title>Dry Samples</title>
    <link rel="stylesheet" href="https://dry.luckeyproductions.nl/drydox.css">
    <style>
    ul {
        column-count: 2;
    }
    body {
        margin: 0;
        width: 100vw;
        height: 100vh;
    }
    div {
        padding: 5vh 5vw;
    }
    ul {
        margin: 0;
    }
    </style>
</head>
<body>
<div>
<ul>
<?php
    $samples = array();

    foreach(scandir(getcwd()) as $n => $file)
        if (strpos($file, ".html"))
            $samples[count($samples)] = $file;

    foreach ($samples as $sample)
        echo "<a href='$sample'><li>$sample</li></a>"."\n";
?>
</ul>
</div>
</body>
</html>
