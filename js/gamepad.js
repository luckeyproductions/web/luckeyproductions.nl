let interval;
interval = setInterval(pollGamepads, 100);

function pollGamepads() {
    const gamepads = navigator.getGamepads();
    for (const gp of gamepads) {
        //    console.log(`Gamepad connected at index ${gp.index}: ${gp.id}. It has ${gp.buttons.length} buttons and ${gp.axes.length} axes.`);
        for (var b = 0; b < gp.buttons.length; ++b) {
            if (gp.buttons[b].pressed) {
                console.log(b);
                switch (b) {
                case 0: case 9: let splashHider = document.getElementById("hideSplash"); if (splashHider) splashHider.checked = true; break;
                case 1: window.location.href = '../'; break;
                case 4: window.history.back(); break;
                case 5: window.history.forward(); break;
                default: break;
                }
            }
        }
    }
}

