<!DOCTYPE html>
<html lang="en" style="background-color: black;">
<?php
    $games = json_decode(file_get_contents("json/games.json"), true);
    $tools = json_decode(file_get_contents("json/tools.json"), true);
    $items = array_merge($games, $tools);

    function tabs(int $n) { $r = ""; while ($n > 0) { $r .= "    "; --$n; } return $r; }
?>
    <head>
        <meta charset="utf-8">
        <title>LucKey Productions</title>
        <meta name="author" content="魔大农" >
        <meta name="subject" content="Games">
        <meta name="image" content="/images/23.svg" />
        <link rel="shortcut icon" href="/images/icon.svg">
        <link rel="me" href="https://c.im/@LucKeyProductions">
        <meta name="abstract" content="This time sink doubles as a source of worlds.">
        <meta name="description" content="Free and open source game development cult aiming to create enchanting games using only FOSS tools.">
        <meta property="og:title" content="LucKey Productions">
        <meta property="og:image" content="/images/23.svg">
        <meta property="og:description" content="Free and open source game development cult aiming to create enchanting games using only FOSS tools.">
        <meta name="keywords" content="Games, Open Source, Free Software, Game Development">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="/css/common.css">
        <link rel="stylesheet" type="text/css" href="/css/main.css">
        <script src="/js/gamepad.js"></script>
        <style>
<?php

    $i = 0;

    foreach ($items as $key => $val)
    {
        if(is_array($val))
        {
            ++$i;
            $length = 0.25 * $i + 0.25;
            $delay  = 0.1  * $i + 1.0;

            echo tabs(2)."#hideSplash:checked ~ #main #items .$key { ";
            echo "animation: rise ".$length."s ".$delay."s  cubic-bezier(1, 1, .23, 1) forwards; ";
            echo "}\n";
        }
    }
    echo "\n";

    foreach ($items as $key => $val)
    {
        if(is_array($val))
        {
            foreach ($val as $name => $value)
            {
                if (!empty($value) && $name == 'icon')
                {
                    echo tabs(2)."#items .$key { ";
                    echo "background-image: url($value); ";
                    echo "}\n";
                }
            }
        }
    }
    echo "\n";

    foreach ($items as $key => $val)
    {
        if(is_array($val))
        {
            foreach ($val as $name => $value)
            {
                if (!empty($value) && $name == 'img')
                {
                    echo tabs(2).".$key.info > *:first-child::before { ";
                    echo "background-image: url($value); ";
                    echo "}\n";
                }
            }
        }
    }

?>
        </style>
    </head>
    <body ontouchstart="" onload="initialScroll()">
        <audio preload><source src="/whoosh.ogg"></audio>
        <audio preload><source src="/plop.ogg"></audio>
        <form>
            <input type="reset"     id="resetSplash"    class="hidden"
                   onclick="new Audio('/plop.ogg').play()"/>
            <input type="checkbox"  id="hideSplash"     class="hidden"
                   onchange="initialScroll(); new Audio('/whoosh.ogg').play()"/>

            <div id="main">
                <div id="items">

                    <div class="oro item" onclick="location.assign('https://luckey.games/pixellab/')"></div>
                    <div class="divider" id="initialScroll"></div>

<?php

    foreach ($games as $key => $val)
    {
        if(is_array($val))
        {
            echo tabs(5)."<label for='$key' class='$key game item'></label>\n";
            echo tabs(5)."<div class='game divider'></div>\n\n";
        }
    }

    foreach ($tools as $key => $val)
    {
        if(is_array($val))
        {
            echo tabs(5)."<label for='$key' class='$key tool item'></label>\n";
            echo tabs(5)."<div class='tool divider'></div>\n\n";
        }
    }

?>
                    <div class="chao item" onclick="location.assign('https://luckey.games/donate')"></div>

                </div>
                <div id="panels">

<?php

    foreach ($items as $key => $val)
    {
        if(is_array($val))
        {
            $icon   = '';
            $title  = '';
            $sub    = '';
            $genre  = '';
            $text   = '';
            $image  = '';
            $lgw    = '';
            $repo   = '';
            $binary = '';
            $aur    = '';

            foreach ($val as $name => $value)
            {
                switch ($name)
                {
                    case 'icon':  $icon   = $value; break;
                    case 'title': $title  = $value; break;
                    case 'sub':   $sub    = $value; break;
                    case 'genre': $genre  = $value; break;
                    case 'text':  $text   = $value; break;
                    case 'img':   $image  = $value; break;
                    case 'lgw':   $lgw    = $value; break;
                    case 'git':   $repo   = $value; break;
                    case 'bin':   $binary = $value; break;
                    case 'aur':   $aur    = $value; break;
                }
            }

            echo tabs(5)."<input type='radio' id='$key' name='items' class='hidden' autocomplete='off'>\n";
            echo tabs(5)."<div class='$key info'>\n";
            echo tabs(6)."<div class='panel'></div>\n";
            echo tabs(6)."<article>\n";
            echo tabs(7)."<header>\n";
            if (!empty($title))         echo tabs(8)."<h1>$title</h1>\n";
            if (!empty($sub))           echo tabs(8)."<h2>$sub</h2>\n";
            if (!empty($genre))         echo tabs(8)."<p><em>$genre</em></p>\n";
            echo tabs(7)."</header>\n";
            echo tabs(7)."<section>\n";
            if (!empty($text))          echo tabs(8)."<p>$text</p>\n";
            echo tabs(7)."</section>\n";
            echo tabs(6)."</article>\n";
            if (!empty($lgw) || !empty($repo) || !empty($binary) || !empty($aur))
            {
                echo tabs(6)."<div class='panelbuttons'>\n";
                if (!empty($lgw))    echo tabs(7)."<a href='https://libregamewiki.org/$lgw'><div class='lgw panelbutton' title='LibreGameWiki'></div></a>\n";
                if (!empty($repo))   echo tabs(7)."<a href='$repo'><div class='beam panelbutton' title='GitLab'></div></a>\n";
                if (!empty($binary)) echo tabs(7)."<a href='$binary'><div class='linux panelbutton' title='Linux 64-bit'></div></a>\n";
                if (!empty($aur))    echo tabs(7)."<a href='https://aur.archlinux.org/packages/$aur'><div class='arch panelbutton' title='AUR'></div></a>\n";
                echo tabs(6)."</div>\n";
            }
            echo tabs(5)."</div>\n\n";
        }
    }

?>
                </div>

                <div id="scrollbar">
                    <input id="midscroll" type="range" min="0" max="1.0" value="0.22" step="0.0005"></input>
                    <div id="leftscroll"></div>
                    <div id="rightscroll"></div>
                </div>
            </div>

            <div id="splash">
                <div id="arc">
                    <img src="/images/arc.png"/>
                    <img id="shade" src="/images/shade.svg"/>
                </div>
                <div>
                    <label class="logo" for="hideSplash"></label>
                    <img id="arch" src="/images/arch.png"/>
                </div>
            </div>

            <label class="logo" for="resetSplash"></label>

            <div class="sidepanel">
                <a href="https://gitlab.com/luckeyproductions">
                    <div class="gitlab" title="GitLab"></div>
                </a>
                <a href="https://com.luckey.games/">
                    <div class="colourship" title="Colourship"></div>
                </a>
            </div>

            <div id="curtain"></div>
        </form>

        <script>

            let scrollbar = document.getElementById("midscroll");
            let items     = document.getElementById("items");

            scrollbar.oninput = function() { scroll(); }
            items.onload  = function() { items.scrollTo(0, 0.22 * factor()); }

            function scroll() {
                items.scrollTo(0, scrollbar.value * factor());
            }

            items.onscroll = function() {
                if (!isActive(scrollbar))
                    scrollbar.value = items.scrollTop / factor();
            }
            function isActive(element) {
                return element.parentElement.querySelector(':active') === element;
            }

            function factor() {
                return items.scrollHeight - items.clientHeight;
            }

            function initialScroll() {
                let scrollElem = document.getElementById("initialScroll");
                items.scrollTo(0, scrollElem.offsetTop + scrollElem.offsetWidth * 0.7);
            }

        </script>
    </body>
</html>
